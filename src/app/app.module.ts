import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import { ConeComponent } from './cone/cone.component';
import { CtwoComponent } from './ctwo/ctwo.component';
import { FbComponent } from './fb/fb.component';

@NgModule({
  declarations: [
    AppComponent,
    ConeComponent,
    CtwoComponent,
    FbComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
