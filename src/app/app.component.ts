import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Control it Travis!!!';
  today = new Date();
  products = [{desc:"Pots", price:12.99},
                {desc:"Dots", price:2.85},
                    {desc:"Spots", price:65.45}];
  myURL= 'https://via.placeholder.com/150';
  flag = true;
  showFlag = false;
  otherId = 'billgates'
  handleClick(){
        console.log(event);
        this.flag=false;
        this.showFlag=true;
  }
  handleCustomEvent(){

        console.log('something happened');
  }
}
